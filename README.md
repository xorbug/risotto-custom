# risotto

risotto is a minimalist, responsive [hugo](https://gohugo.io) theme inspired by terminal ricing aesthetics.

![Screenshot of the risotto theme](https://raw.githubusercontent.com/joeroe/risotto/master/images/screenshot.png)

risotto-custom introduces the following changes:
- show additional post infos in the side content (ToC, series, tags, categories)
- move to [Fork Awesome][forkawesome] icon set (more community contributed icons, 100% free)
- show dates of entries when listing contents
- provide default 404 page and posts archetype
- other minor changes

[forkawesome]:      https://forkaweso.me

## Install

The easiest way to install the theme is to clone this repository into your site's `themes` directory:

```shell
git clone https://gitlab.com/xorbug/risotto-custom.git themes/risotto-custom
```

If your site is already a git repository, you can add the theme as a submodule instead:

```shell
git submodule add https://gitlab.com/xorbug/risotto-custom.git themes/risotto-custom
```

## Configure

To use the theme, add `theme = risotto-custom` to your site's `config.toml` or `config.yaml`.

See `exampleSite/config.toml` for the theme-specific parameters you need to add to your site's `config.toml` or `config.yaml` to configure the theme.

## Update

If you installed the theme using `git clone`, pull the repository to get the latest version:

```shell
cd themes/risotto-custom
git pull
```

Or, if you added it as a git submodule:

```shell
git submodule update --remote
```
